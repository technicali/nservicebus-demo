
using InsightBusInfrastructure;

namespace ExecutionSystemResponseListener.Config
{
    public class EndpointConfig : NServiceBusHostedEndpointConfigBase
    {
        public override string EndpointName
        {
            get { return "ExecutionSystemResponseListener"; }
        }
    }
}
