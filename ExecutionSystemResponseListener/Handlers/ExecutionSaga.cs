﻿using System;
using NServiceBus;
using NServiceBus.Saga;
using OtcTradeBusMessages.Events;
using OtcTradeBusMessages.Messages;

namespace ExecutionSystemResponseListener.Handlers
{
    public class ExecutionSagaBase : Saga<OtcTradeExecutionSagaData>, 
        IAmStartedByMessages<IExecutionCancelled>, 
        IAmStartedByMessages<IExecutionAmended>,
        IAmStartedByMessages<IExecutionCreated>,
        IHandleMessages<IExecutionSentToNtEvent>,
        IHandleMessages<OtcPricingDatabaseUpdated>
    {
        protected override void ConfigureHowToFindSaga(SagaPropertyMapper<OtcTradeExecutionSagaData> mapper)
        {
            mapper.ConfigureMapping<IExecutionCancelled>(m => m.PositionIdentifiers)
                .ToSaga(s => s.PositionIdentifiers);

            mapper.ConfigureMapping<IExecutionAmended>(m => m.PositionIdentifiers)
                .ToSaga(s => s.PositionIdentifiers);

            mapper.ConfigureMapping<IExecutionCreated>(m => m.PositionIdentifiers)
                .ToSaga(s => s.PositionIdentifiers);

            mapper.ConfigureMapping<IExecutionSentToNtEvent>(m => m.PositionIdentifiers)
                .ToSaga(s => s.PositionIdentifiers);

            mapper.ConfigureMapping<OtcPricingDatabaseUpdated>(m => m.PositionIdentifiers)
                .ToSaga(s => s.PositionIdentifiers);
        }

        public void Handle(IExecutionSentToNtEvent message)
        {
            Console.WriteLine("IExecutionSentToNtEvent event received");
            Data.ExecutionSentToNtDateTime = DateTime.Now;
            ProcessMessage("IExecutionSentToNtEvent");
        }

        public void Handle(OtcPricingDatabaseUpdated message)
        {
            Console.WriteLine("OtcPricingDatabaseUpdated event received");
            Data.OtcPricingDataUpdatedDateTime = DateTime.Now;
            ProcessMessage("OtcPricingDatabaseUpdated");
        }

        public void Handle(IExecutionCancelled message)
        {
            Handle(message, ExecutionActionType.Cancelled, "IExecutionCancelled");
        }

        public void Handle(IExecutionAmended message)
        {
            Handle(message, ExecutionActionType.Amended, "IExecutionAmended");
        }

        public void Handle(IExecutionCreated message)
        {
            Handle(message, ExecutionActionType.Created, "IExecutionCreated");
        }

        private void Handle(IOtcTradeEvent message, ExecutionActionType actionType, string startingEvent)
        {
            Console.WriteLine("{0} event received", startingEvent);
            Data.PositionIdentifiers = message.PositionIdentifiers;
            Data.EventCreatedDateTime = message.EventDateTime;
            Data.ActionType = actionType;
            ProcessMessage(startingEvent);
        }

        private void ProcessMessage(string startingEvent)
        {
            Console.WriteLine("Checking if the {0} event is fully processed", startingEvent);
            bool completed = Data.ExecutionSentToNtDateTime.HasValue &&
                             Data.OtcPricingDataUpdatedDateTime.HasValue;

            if (completed)
            {
                EventProcessedBase responseData;
                if (TryCreateResponseData(out responseData))
                {
                    string responseEvent = responseData.GetType().Name;
                    Console.WriteLine("{0} event fully processed. Sending {1} response to originator.", startingEvent, responseEvent);
                    responseData.PositionIdentifiers = Data.PositionIdentifiers;
                    ReplyToOriginator(responseData);
                }

                MarkAsComplete();
            }
        }

        private bool TryCreateResponseData(out EventProcessedBase responseData)
        {
            responseData = null;

            switch (Data.ActionType)
            {
                case ExecutionActionType.Amended:
                    responseData = new ExecutionAmendedProcessed();
                    break;
                case ExecutionActionType.Cancelled:
                    responseData = new ExecutionCancelledProcessed();
                    break;
                case ExecutionActionType.Created:
                    responseData = new ExecutionCreatedProcessed();
                    break;
            }

            return responseData != null;
        }
    }
}