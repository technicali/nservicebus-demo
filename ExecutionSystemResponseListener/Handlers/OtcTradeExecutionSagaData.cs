﻿using System;
using NServiceBus.Saga;
using OtcTradeBusMessages.Models;

namespace ExecutionSystemResponseListener.Handlers
{
    public class OtcTradeExecutionSagaData : ContainSagaData
    {
        public virtual OtcPositionIdentifiers PositionIdentifiers { get; set; }
        public virtual DateTime EventCreatedDateTime { get; set; }
        public virtual ExecutionActionType ActionType { get; set; }
        public virtual DateTime? ExecutionSentToNtDateTime { get; set; }
        public virtual DateTime? OtcPricingDataUpdatedDateTime { get; set; }
    }

    public enum ExecutionActionType
    {
        Created,
        Amended,
        Cancelled
    }
}