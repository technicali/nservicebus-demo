﻿using System;
using InsightBusInfrastructure.Shared.Models;

namespace InsightBusInfrastructure.Shared.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class BusMessageAttribute : Attribute
    {
        public BusMessageAttribute(BusMessageType messageType)
        {
            MessageType = messageType;
        }

        public BusMessageType MessageType { get; private set; }
    }
}
