﻿using System;

namespace InsightBusInfrastructure.Shared.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DataBusAttribute : Attribute
    {
    }
}
