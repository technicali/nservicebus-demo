﻿using System;

namespace InsightBusInfrastructure.Shared.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class TimeToReceiveAttribute : Attribute
    {
        public TimeToReceiveAttribute(string timeSpan)
        {
            TimeSpan timeToReceive;
            bool parsed = TimeSpan.TryParse(timeSpan, out timeToReceive);
            TimeToReceive = parsed ? timeToReceive : TimeSpan.MaxValue;
        }

        public TimeSpan TimeToReceive { get; private set; }
    }
}
