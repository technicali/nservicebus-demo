﻿namespace InsightBusInfrastructure.Shared.Models
{
    public enum BusMessageType
    {
        Command,
        Event,
        Message
    }
}
