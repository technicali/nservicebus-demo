﻿using System;
using System.Linq;
using System.Reflection;
using InsightBusInfrastructure.Shared.Attributes;
using InsightBusInfrastructure.Shared.Models;
using NServiceBus;

namespace InsightBusInfrastructure
{
    public class Conventions : INeedInitialization
    {
        private static bool IsBusMessageType(Type t, BusMessageType messageType)
        {
            var attribute = t.GetCustomAttributes(true)
                .OfType<BusMessageAttribute>()
                .FirstOrDefault();

            return attribute != null && attribute.MessageType == messageType;
        }

        private static bool IsDataBusProperty(PropertyInfo pi)
        {
            return pi.GetCustomAttributes(true)
                    .OfType<DataBusAttribute>()
                    .Any();
        }

        private static TimeSpan GetTimeToReceive(Type t)
        {
            var attribute = t.GetCustomAttributes(true)
                .OfType<TimeToReceiveAttribute>()
                .FirstOrDefault();

            return attribute != null ? attribute.TimeToReceive : TimeSpan.MaxValue;
        }

        public void Customize(BusConfiguration configuration)
        {
            configuration.Conventions()
                .DefiningCommandsAs(t => IsBusMessageType(t, BusMessageType.Command))
                .DefiningEventsAs(t => IsBusMessageType(t, BusMessageType.Event))
                .DefiningMessagesAs(t => IsBusMessageType(t, BusMessageType.Message))
                .DefiningTimeToBeReceivedAs(GetTimeToReceive)
                .DefiningDataBusPropertiesAs(IsDataBusProperty);
        }
    }
}