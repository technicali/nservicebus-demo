﻿using NServiceBus;
using NServiceBus.Features;

namespace InsightBusInfrastructure
{
    public abstract class EndpointConfigBase
    {
        public abstract string EndpointName { get; }

        protected virtual void CustomizeBus(BusConfiguration configuration)
        {
        }

        protected void Initialize(BusConfiguration configuration)
        {
            // NServiceBus provides the following durable storage options
            // To use RavenDB, install-package NServiceBus.RavenDB and then use configuration.UsePersistence<RavenDBPersistence>();
            // To use SQLServer, install-package NServiceBus.NHibernate and then use configuration.UsePersistence<NHibernatePersistence>();

            // If you don't need a durable storage you can also use, configuration.UsePersistence<InMemoryPersistence>();
            // more details on persistence can be found here: http://docs.particular.net/nservicebus/persistence-in-nservicebus

            //Also note that you can mix and match storages to fit you specific needs. 
            //http://docs.particular.net/nservicebus/persistence-order
            configuration.EnableInstallers();
            // SQL connection string: @"Data Source=.\SQLEXPRESS;Initial Catalog=ServiceBus;Integrated Security=True"
            configuration.UsePersistence<InMemoryPersistence>();
            configuration.UseTransport<MsmqTransport>();
            configuration.EnableFeature<SecondLevelRetries>();

            CustomizeBus(configuration);
            configuration.EndpointName(EndpointName);
        }
    }
}