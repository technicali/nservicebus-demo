﻿using NServiceBus;

namespace InsightBusInfrastructure
{
    public abstract class NServiceBusHostedEndpointConfigBase : EndpointConfigBase, IConfigureThisEndpoint
    {
        /*
        This class configures this endpoint as a Server. More information about how to configure the NServiceBus host
        can be found here: http://particular.net/articles/the-nservicebus-host
        */
        public void Customize(BusConfiguration configuration)
        {
            Initialize(configuration);
        }
    }
}