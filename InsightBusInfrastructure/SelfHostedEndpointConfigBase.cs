﻿using NServiceBus;

namespace InsightBusInfrastructure
{
    public abstract class SelfHostedEndpointConfigBase : EndpointConfigBase, INeedInitialization
    {
        public void Customize(BusConfiguration configuration)
        {
            Initialize(configuration);
        }
    }
}