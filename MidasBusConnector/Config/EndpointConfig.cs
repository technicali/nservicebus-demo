
using InsightBusInfrastructure;

namespace MidasBusConnector.Config
{
    public class EndpointConfig : NServiceBusHostedEndpointConfigBase
    {
        public override string EndpointName { get { return "MidasBusConnector"; } }
    }
}
