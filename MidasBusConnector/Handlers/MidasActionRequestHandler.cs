﻿using System;
using NServiceBus;
using OtcTradeBusMessages.Events;

namespace MidasBusConnector.Handlers
{
    public class MidasActionRequestHandler : IHandleMessages<IExecutionCreated>, IHandleMessages<IExecutionCancelled>
    {
        public IBus Bus { get; set; }

        public void Handle(IExecutionCreated message)
        {
            Console.WriteLine("Received IExecutionCreated response message");
            Bus.Publish<IInsightIdCreated>(data => data.PositionIdentifiers = message.PositionIdentifiers);
        }

        public void Handle(IExecutionCancelled message)
        {
            Console.WriteLine("Received IExecutionCancelled response message");
        }
    }
}