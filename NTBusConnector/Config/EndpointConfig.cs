
using InsightBusInfrastructure;

namespace NTBusConnector.Config
{
    public class EndpointConfig : NServiceBusHostedEndpointConfigBase
    {
        public override string EndpointName { get { return "NTBusConnector"; } }
    }
}
