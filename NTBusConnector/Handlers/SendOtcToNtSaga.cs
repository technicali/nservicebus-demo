﻿using System;
using NServiceBus;
using NServiceBus.Saga;
using OtcTradeBusMessages.Events;

namespace NTBusConnector.Handlers
{
    public class SendOtcToNtSaga : Saga<SendOtcToNtSagaData>, 
        IAmStartedByMessages<IExecutionAmended>,
        IAmStartedByMessages<IExecutionCancelled>,
        IAmStartedByMessages<IExecutionCreated>,
        IHandleMessages<OtcPricingDatabaseUpdated>
    {
        protected override void ConfigureHowToFindSaga(SagaPropertyMapper<SendOtcToNtSagaData> mapper)
        {
            mapper.ConfigureMapping<IExecutionAmended>(e => e.PositionIdentifiers).ToSaga(s => s.PositionIdentifiers);
            mapper.ConfigureMapping<IExecutionCancelled>(e => e.PositionIdentifiers).ToSaga(s => s.PositionIdentifiers);
            mapper.ConfigureMapping<IExecutionCreated>(e => e.PositionIdentifiers).ToSaga(s => s.PositionIdentifiers);
            mapper.ConfigureMapping<OtcPricingDatabaseUpdated>(e => e.PositionIdentifiers).ToSaga(s => s.PositionIdentifiers);
        }

        public void Handle(OtcPricingDatabaseUpdated message)
        {
            Console.WriteLine("OtcPricingDatabaseUpdated event received");
            Data.OtcPricingDataUpdatedDateTime = DateTime.Now;
            ProcessMessage("OtcPricingDatabaseUpdated");
        }

        public void Handle(IExecutionAmended message)
        {
            Handle(message, "IExecutionAmended");
        }

        public void Handle(IExecutionCancelled message)
        {
            Handle(message, "IExecutionCancelled");
        }

        public void Handle(IExecutionCreated message)
        {
            Handle(message, "IExecutionCreated");
        }

        private void Handle(IOtcTradeEvent message, string startingEvent)
        {
            Console.WriteLine("{0} event received", startingEvent);
            Data.PositionIdentifiers = message.PositionIdentifiers;
            Data.EventCreatedDateTime = message.EventDateTime;
            ProcessMessage(startingEvent);
        }

        private void ProcessMessage(string startingEvent)
        {
            Console.WriteLine("Checking if the {0} event is fully processed", startingEvent);
            bool completed = Data.OtcPricingDataUpdatedDateTime.HasValue;

            if (completed)
            {
                Console.WriteLine("{0} event fully processed. Publishing IExecutionSentToNtEvent to subscribers.", startingEvent);
                Bus.Publish<IExecutionSentToNtEvent>(data => data.PositionIdentifiers = Data.PositionIdentifiers);
                MarkAsComplete();
            }
        }
    }
}