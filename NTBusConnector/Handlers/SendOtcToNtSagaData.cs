﻿using System;
using NServiceBus.Saga;
using OtcTradeBusMessages.Models;

namespace NTBusConnector.Handlers
{
    public class SendOtcToNtSagaData : ContainSagaData
    {
        public virtual OtcPositionIdentifiers PositionIdentifiers { get; set; }
        public virtual DateTime EventCreatedDateTime { get; set; }
        public virtual DateTime? OtcPricingDataUpdatedDateTime { get; set; }
    }
}