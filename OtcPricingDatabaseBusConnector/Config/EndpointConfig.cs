
using InsightBusInfrastructure;

namespace OtcPricingDatabaseBusConnector.Config
{
    public class EndpointConfig : NServiceBusHostedEndpointConfigBase
    {
        public override string EndpointName { get { return "OtcPricingDatabaseBusConnector"; } }
    }
}
