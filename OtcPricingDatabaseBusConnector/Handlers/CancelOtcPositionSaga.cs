﻿using OtcTradeBusMessages.Events;

namespace OtcPricingDatabaseBusConnector.Handlers
{
    class CancelOtcPositionSaga : UpdateOtcPositionSagaBase<IExecutionCancelled>
    {
    }
}