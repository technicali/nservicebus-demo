﻿using System;
using NServiceBus;
using NServiceBus.Saga;
using OtcTradeBusMessages.Events;

namespace OtcPricingDatabaseBusConnector.Handlers
{
    public class CreateOtcPositionSaga : UpdateOtcPositionSagaBase<IExecutionCreated>,
        IHandleMessages<IInsightIdCreated>
    {
        protected override void ConfigureHowToFindSaga(SagaPropertyMapper<OtcPricingDataUpdateSagaData> mapper)
        {
            base.ConfigureHowToFindSaga(mapper);
            mapper.ConfigureMapping<IInsightIdCreated>(m => m.PositionIdentifiers).ToSaga(s => s.PositionIdentifiers);
        }

        protected override void HandleStartingMessage(IExecutionCreated message)
        {
            Data.RequiresMidasResponse = true;
        }

        public void Handle(IInsightIdCreated message)
        {
            Console.WriteLine("CreateOtcPositionSaga: IInsightIdCreated event received");
            Data.InsightIdCreatedDateTime = DateTime.Now;
            ProcessMessage();
        }
    }
}