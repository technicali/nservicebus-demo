﻿using System;
using NServiceBus.Saga;
using OtcTradeBusMessages.Models;

namespace OtcPricingDatabaseBusConnector.Handlers
{
    public class OtcPricingDataUpdateSagaData : ContainSagaData
    {
        [Unique]
        public virtual OtcPositionIdentifiers PositionIdentifiers { get; set; }
        public virtual DateTime EventCreatedDateTime { get; set; }
        public virtual bool RequiresMidasResponse { get; set; }
        public virtual DateTime? InsightIdCreatedDateTime { get; set; }
    }
}