﻿using System;
using NServiceBus.Saga;
using OtcTradeBusMessages.Events;

namespace OtcPricingDatabaseBusConnector.Handlers
{
    public abstract class UpdateOtcPositionSagaBase<TStartingEvent> : Saga<OtcPricingDataUpdateSagaData>, 
        IAmStartedByMessages<TStartingEvent>
        where TStartingEvent : IOtcTradeEvent
    {
        protected override void ConfigureHowToFindSaga(SagaPropertyMapper<OtcPricingDataUpdateSagaData> mapper)
        {
            mapper.ConfigureMapping<TStartingEvent>(e => e.PositionIdentifiers).ToSaga(s => s.PositionIdentifiers);
        }

        protected virtual void HandleStartingMessage(TStartingEvent message)
        {
            Data.RequiresMidasResponse = false;
        }

        public void Handle(TStartingEvent message)
        {
            string startingEvent = typeof(TStartingEvent).Name;
            Console.WriteLine("{0} event received", startingEvent);
            Data.PositionIdentifiers = message.PositionIdentifiers;
            Data.EventCreatedDateTime = message.EventDateTime;
            HandleStartingMessage(message);
            ProcessMessage();
        }

        protected void ProcessMessage()
        {
            string startingEvent = typeof (TStartingEvent).Name;
            Console.WriteLine("Checking if the {0} event is fully processed", startingEvent);
            bool completed = !Data.RequiresMidasResponse || Data.InsightIdCreatedDateTime.HasValue;

            if (completed)
            {
                Console.WriteLine("{0} event fully processed. Publishing OtcPricingDatabaseUpdated to subscribers.", startingEvent);
                Bus.Publish<OtcPricingDatabaseUpdated>(data => data.PositionIdentifiers = Data.PositionIdentifiers);
                MarkAsComplete();
            }
        }
    }
}