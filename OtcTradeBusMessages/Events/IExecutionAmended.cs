﻿using InsightBusInfrastructure.Shared.Attributes;
using InsightBusInfrastructure.Shared.Models;

namespace OtcTradeBusMessages.Events
{
    [BusMessage(BusMessageType.Event)]
    public interface IExecutionAmended : IOtcTradeEvent
    {
    }
}