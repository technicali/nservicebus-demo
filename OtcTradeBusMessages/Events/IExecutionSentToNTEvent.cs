﻿using System;
using InsightBusInfrastructure.Shared.Attributes;
using InsightBusInfrastructure.Shared.Models;
using OtcTradeBusMessages.Models;

namespace OtcTradeBusMessages.Events
{
    [BusMessage(BusMessageType.Event)]
    public interface IExecutionSentToNtEvent
    {
        OtcPositionIdentifiers PositionIdentifiers { get; set; }
        DateTime ExecutionSentDateTime { get; set; }
    }
}