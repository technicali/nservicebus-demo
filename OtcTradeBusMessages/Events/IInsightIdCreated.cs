﻿using InsightBusInfrastructure.Shared.Attributes;
using InsightBusInfrastructure.Shared.Models;
using OtcTradeBusMessages.Models;

namespace OtcTradeBusMessages.Events
{
    [BusMessage(BusMessageType.Event)]
    public interface IInsightIdCreated
    {
        int InsightId { get; set; }
        OtcPositionIdentifiers PositionIdentifiers { get; set; }
    }
}