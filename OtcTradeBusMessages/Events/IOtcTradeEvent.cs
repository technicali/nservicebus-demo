﻿using System;
using InsightBusInfrastructure.Shared.Attributes;
using InsightBusInfrastructure.Shared.Models;
using OtcTradeBusMessages.Models;

namespace OtcTradeBusMessages.Events
{
    [BusMessage(BusMessageType.Event)]
    public interface IOtcTradeEvent
    {
        string EventSourceSystem { get; set; }
        string EventUser { get; set; }
        DateTime EventDateTime { get; set; }
        OtcPositionIdentifiers PositionIdentifiers { get; set; }
        OtcTradeMessage Payload { get; set; } 
    }
}