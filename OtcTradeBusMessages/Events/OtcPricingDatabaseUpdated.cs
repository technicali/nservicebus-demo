﻿using InsightBusInfrastructure.Shared.Attributes;
using InsightBusInfrastructure.Shared.Models;
using OtcTradeBusMessages.Models;

namespace OtcTradeBusMessages.Events
{
    [BusMessage(BusMessageType.Event)]
    public class OtcPricingDatabaseUpdated
    {
        public OtcPositionIdentifiers PositionIdentifiers { get; set; }
    }
}