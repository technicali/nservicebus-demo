﻿using OtcTradeBusMessages.Models;

namespace OtcTradeBusMessages.Messages
{
    public abstract class EventProcessedBase
    {
        public OtcPositionIdentifiers PositionIdentifiers { get; set; }
    }
}