﻿using InsightBusInfrastructure.Shared.Attributes;
using InsightBusInfrastructure.Shared.Models;

namespace OtcTradeBusMessages.Messages
{
    [BusMessage(BusMessageType.Message)]
    public class ExecutionCancelledProcessed : EventProcessedBase
    {
    }
}