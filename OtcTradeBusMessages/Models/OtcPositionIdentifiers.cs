﻿using System;

namespace OtcTradeBusMessages.Models
{
    public class OtcPositionIdentifiers : IEquatable<OtcPositionIdentifiers>
    {
        public string Sedol { get; set; }
        public string HiportCode { get; set; }
        public int SapphireId { get; set; }

        public bool Equals(OtcPositionIdentifiers other)
        {
            if (other == null)
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return StringComparer.InvariantCultureIgnoreCase.Equals(HiportCode, other.HiportCode) &&
                   StringComparer.InvariantCultureIgnoreCase.Equals(Sedol, other.Sedol) &&
                   SapphireId == other.SapphireId;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as OtcPositionIdentifiers);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Sedol != null ? Sedol.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (HiportCode != null ? HiportCode.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ SapphireId;
                return hashCode;
            }
        }

        public override string ToString()
        {
            return HiportCode;
        }
    }
}