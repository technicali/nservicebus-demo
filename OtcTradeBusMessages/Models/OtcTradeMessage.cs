﻿using System;

namespace OtcTradeBusMessages.Models
{
    public class OtcTradeMessage
    {
        public string FundManagerNotes { get; set; }
        public DateTime TradeDateTime { get; set; }
    }
}