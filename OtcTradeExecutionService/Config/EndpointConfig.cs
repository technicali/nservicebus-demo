
using InsightBusInfrastructure;

namespace OtcTradeExecutionService.Config
{
    public class EndpointConfig : SelfHostedEndpointConfigBase
    {
        public override string EndpointName { get { return "OtcTradeExecutionService"; } }
    }
}
