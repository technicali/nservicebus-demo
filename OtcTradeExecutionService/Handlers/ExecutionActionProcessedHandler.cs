﻿using System;
using NServiceBus;
using OtcTradeBusMessages.Messages;

namespace OtcTradeExecutionService.Handlers
{
    public class ExecutionActionProcessedHandler : 
        IHandleMessages<ExecutionCreatedProcessed>,
        IHandleMessages<ExecutionAmendedProcessed>,
        IHandleMessages<ExecutionCancelledProcessed>
    {

        public void Handle(ExecutionCreatedProcessed message)
        {
            Console.WriteLine("Received ExecutionCreatedProcessed response message");
        }

        public void Handle(ExecutionAmendedProcessed message)
        {
            Console.WriteLine("Received ExecutionAmendedProcessed response message");
        }

        public void Handle(ExecutionCancelledProcessed message)
        {
            Console.WriteLine("Received ExecutionCancelledProcessed response message");
        }
    }
}