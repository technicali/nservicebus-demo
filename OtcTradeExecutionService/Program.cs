﻿using System;
using System.Collections.Generic;
using System.Threading;
using NServiceBus;
using OtcTradeBusMessages.Events;
using OtcTradeBusMessages.Models;

namespace OtcTradeExecutionService
{
    public class Program
    {
        private const string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private readonly Random _generator = new Random();
        private int _sapphireIdSeed = 213345;

        public IBus Bus { get; set; }

        internal static void Main()
        {
            Program program = new Program();
            BusConfiguration configuration = new BusConfiguration();
            program.Bus = NServiceBus.Bus.Create(configuration).Start();

            while (true)
            {
                Console.WriteLine("Choose message action and press <RETURN> to publish:");
                Console.WriteLine("N - New execution");
                Console.WriteLine("A - Amend execution");
                Console.WriteLine("C - Cancel execution");
                Console.WriteLine("X - Exit");

                string input = Console.ReadLine();
                if (string.IsNullOrEmpty(input) || input == "X")
                {
                    break;
                }

                switch (input.ToUpper())
                {
                    case "N":
                        program.PublishExecutionCreatedEventData();
                        break;
                    case "A":
                        program.PublishExecutionAmendedEventData();
                        break;
                    case "C":
                        program.PublishExecutionCancelledEventData();
                        break;
                }
            }
        }

        private void PublishExecutionCreatedEventData()
        {
            Console.WriteLine("Publishing an execution created event on the bus");
            Bus.Publish<IExecutionCreated>(UpdateMessageProperties);
        }

        private void PublishExecutionAmendedEventData()
        {
            Console.WriteLine("Publishing an execution amended event on the bus");
            Bus.Publish<IExecutionAmended>(UpdateMessageProperties);
        }

        private void PublishExecutionCancelledEventData()
        {
            Console.WriteLine("Publishing an execution cancelled event on the bus");
            Bus.Publish<IExecutionCancelled>(UpdateMessageProperties);
        }

        private void UpdateMessageProperties(IOtcTradeEvent message)
        {
            string sedol = string.Format("{0}{1}{2}", GenerateRandomWord(2), GenerateNumericWord(3), GenerateRandomWord(3));
            message.EventDateTime = DateTime.Now;
            message.EventUser = GenerateRandomWord(6);
            message.PositionIdentifiers = new OtcPositionIdentifiers
            {
                Sedol = sedol,
                HiportCode = "D" + sedol,
                SapphireId = Interlocked.Increment(ref _sapphireIdSeed)
            };
        }

        private string GenerateRandomWord(int length)
        {
            return string.Join(string.Empty, GetRandomCharacters(length));
        }

        private string GenerateNumericWord(int length)
        {
            return string.Join(string.Empty, GetRandomNumbers(length));
        }

        private IEnumerable<string> GetRandomCharacters(int length)
        {
            for (int i = 0; i < length; i++)
            {
                int randIndex = _generator.Next(26);
                yield return Alphabet[randIndex].ToString();
            }
        }

        private IEnumerable<int> GetRandomNumbers(int length)
        {
            for (int i = 0; i < length; i++)
            {
                yield return _generator.Next(10);
            }
        }
    }
}